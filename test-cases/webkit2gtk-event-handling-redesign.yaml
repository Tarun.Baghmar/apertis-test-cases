metadata:
  name: webkit2gtk-event-handling-redesign
  format: "Apertis Test Definition 1.0"
  image-types:
    target:  [ armhf-internal, armhf, amd64 ]
    basesdk: [ amd64 ]
    sdk:     [ amd64 ]
  image-deployment:
    - APT
  type: functional
  exec-type: manual
  priority: medium
  maintainer: "Apertis Project"
  description: "Test WebKit2Gtk ability to scroll by touch with inertial behaviour
                after the touch ended."

  resources:
    - "A touch-screen with multiple touch points."

  expected:
    - "A page with many nested scrollable elements should be rendered."
    - "Dragging the right side of the page with a single touch or mouse click
       should cause the page content to scroll."
    - "Clicking or touching the entry should focus it and allow you to enter text."
    - "Scrolling the area bellow the entry should scroll the content that is
       inside that area only, revealing another text entry."
    - "Clicking the entry that is inside the scrollable area should focus it and
       allow you to enter text."
    - "When scrolling to the bottom of the page with a quick drag and release
       touch with enough momentum should cause the scroll to continue with
       decreasing speed towards the bottom of the page."
    - "Starting a touch drag from the green \"Touch scroll drags here should not
       start\" element should not result in page scrolls."
    - "Starting a mouse drag from the red \"Mouse scroll drags here should not
       start\" element should not result in page scrolls."
    - "Clicking one of the links at the bottom of the page should take you to the
       appropriate page (www.collabora.com or planet.collabora.com)."

install:
  git-repos:
    - url: https://gitlab.apertis.org/tests/webkit2gtk.git
      branch: 'apertis/v2021dev1'

run:
  steps:
    - "Run the following command:"
    - $ /usr/lib/*/webkit2gtk-4.0/MiniBrowser -g 700x600 webkit2gtk/scroll.html
    - "Wait for the page to load, there will be two in-page scrollable areas,
       with a text entry in between them."
    - "Scroll the main page until you align the top of the text entry with the
       top of the viewport."
    - "Click the entry to focus it and enter some text."
    - "Scroll the scrollable area below the entry until you find another entry
       inside the scrollable area."
    - "Try to focus it and enter some text."
    - "Scroll to the bottom of the main page by doing a quick drag and release."
    - "If you have a touchscreen, try dragging from the green area."
    - "If you have a mouse, try dragging from the red area."
    - "Click one of the two links at the very bottom and wait for the page
       to load."
