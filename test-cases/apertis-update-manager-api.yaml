metadata:
  name: apertis-update-manager-api
  format: "Apertis Test Definition 1.0"
  image-types:
    minimal: [ armhf ]
  image-deployment:
    - OSTree
  type: functional
  exec-type: manual
  priority: low
  maintainer: "Apertis Project"
  description: "Test the apertis-update-manager automatic update.
                The automated version of this test: https://qa.apertis.org/aum-api.html"

  resources:
    - "A static update bundle file of the same architecture, variant and version as the testing image"
    - "A Fat32 USB flash drive, preloaded with the update bundle at the root of the disk"
    - "The latest static update file can be downloaded at the same location than the target image. It has the same basename, and a '.delta' extension"
    - "The static update file should be copied to the flash drive using the name 'static-update.bundle'."
    - "A PC must be connected to DUT serial port"

  expected:
    - "CLI utility `updatectl` is able to interact with Apertis update manager"
    - "The update was properly applied"

run:
  steps:
    - "Check the initial deployment"
    - $ sudo ostree admin status
    - "Prepare the copy of commit and deploy to allow the upgrade to the same version"
    - "Command below shows you an initial commit ID, for instance"
    - |
        $ export BOOTID=$(sudo ostree admin status | sed -n -e 's/^\* apertis \([0-9a-f]*\)\.[0-9]$/\1/p'); echo $BOOTID
    - "Get the Collection ID and ref"
    -  $ export CID=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 1 -d ' '); echo COLLECTION_ID=$CID
    -  $ export REF=$(sudo ostree refs -c | head -n 1 | tr -d '(),' | cut -f 2 -d ' '); echo REF=$REF
    - "Create the list of files to skip and enshure there are some files in these directories"
    - $ ls -1d /usr/share/locale /usr/share/man /usr/share/zoneinfo > /tmp/skip
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Create the commit with changed timestamp and skipped list from above to allow upgrade with recent update file"
    - |
        $ export NEWID=$(sudo ostree commit --orphan --tree=ref=$BOOTID --add-metadata-string=ostree.collection-binding=$CID --bind-ref=$REF --timestamp="1 year ago" --skip-list=/tmp/skip); echo "New commit: $NEWID"
    - "Deploy the prepared commit"
    - $ sudo ostree admin upgrade --allow-downgrade --deploy-only --override-commit=$NEWID --reboot
    - "Wait until the system is booted again and check the deployment"
    - $ sudo ostree admin status
    - "The booted commit (started with '*') must have ID which we prepare and the initial commit ID should be marked as '(rollback)'"
    - "Check booted deployment have no file objects which we skip"
    - $ du -sh /usr/share/locale /usr/share/man /usr/share/zoneinfo
    - "Remove the initial deployment"
    - $ sudo ostree admin undeploy 1
    - "Reboot the system"
    - "Check the current deployment"
    - $ sudo ostree admin status
    - "Start the user interface agent with mode preventing automatic system reboot after update"
    - $ sudo updatectl --register-upgrade-handler &
    - "Plug the USB flash drive into the device"
    - "The update starts automatically"
    - "After the update, the device does *not* reboot automatically"
    - "Check that the user interface agent reports the pending update"
    - |
        >** Message: Upgrade status: Checking
         ** Message: An upgrade is pending
    - "Remove the USB flash drive"
    - "Check if there is pending deployment and reboot the DUT"
    - $ sudo ostree admin status
    - $ sudo reboot
    - "Check the current deployment has been updated and that the rollback entry points to the prepared deployment"
    - $ sudo ostree admin status
    - "Start the user interface agent"
    - $ sudo updatectl &
    - "Plug the USB flash drive with the same update file into the device"
    - "Check that the user interface agent reports the system is up to update"
    - |
        >** Message: Upgrade status: Checking
        ** Message: System is up to date
